package com.example.zpravamessenger.zprava.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.zpravamessenger.zprava.ApiCaller;
import com.example.zpravamessenger.zprava.R;
import com.example.zpravamessenger.zprava.SaveSharedPreference;
import com.example.zpravamessenger.zprava.User;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {
    // class vars
    private ApiCaller apiCaller;
    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        this.apiCaller = new ApiCaller(this);
    }

    // helper method to check password accurate
    private boolean validPassword(String pw, String pw2){
        return pw.equals(pw2);
    }

    // helper method to ensure no blank entries
    private boolean validInput(String username, String firstName, String lastName, String email){
        return (
                !username.equals("")
                && !firstName.equals("")
                && !lastName.equals("")
                && (!email.equals("") && email.contains("@"))
        );
    }

    public void signupFunction(View view){
        EditText usernameEditText = findViewById(R.id.usernameEditText);
        EditText firstNameEditText = findViewById(R.id.firstNameEditText);
        EditText lastNameEditText = findViewById(R.id.lastNameEditText);
        EditText emailEditText = findViewById(R.id.emailEditText);
        EditText passwordEditText = findViewById(R.id.passwordEditText);
        EditText confirmPasswordEditText = findViewById(R.id.confirmPasswordEditText);

        username = usernameEditText.getText().toString();
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();
        // dummy var for now
        String phoneNumber = "N/A";


        if (validInput(username, firstName, lastName, email) && validPassword(password, confirmPassword)) {
            // create map of user data
            Map<String, String> userData = new HashMap<>();
            userData.put("username", username);
            userData.put("first_name", firstName);
            userData.put("last_name", lastName);
            userData.put("phone", phoneNumber);
            userData.put("email", email);
            userData.put("password", password);

            Log.d("TEST", userData.toString());
            apiCaller.createUser(this, userData);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Invalid Entries...", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void callAutoLogin(User user){
        if (user != null) {
            SaveSharedPreference.setUserId(this, Integer.toString(user.getUserId()));
            Map<String, String> creds = new HashMap<>();
            creds.put("username", username);
            creds.put("password", password);
            apiCaller.autoLogin(this, creds);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Unable to create new user.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void setAuthToken(String token) {
        SaveSharedPreference.setUserName(this, username);
        SaveSharedPreference.setAuthToken(this, token);
        apiCaller.getUserInfoSignup(this);
    }

    public void setUserId(User user) {
        SaveSharedPreference.setUserId(this, String.valueOf(user.getUserId()));
        Intent intent = new Intent(getApplicationContext(), ConversationListActivity.class);
        startActivity(intent);
    }

    public void onError(String error){
        Toast toast = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG);
        toast.show();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
}
