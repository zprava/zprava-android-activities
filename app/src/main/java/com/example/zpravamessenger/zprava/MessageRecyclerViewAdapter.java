package com.example.zpravamessenger.zprava;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "MessageRecyclerViewAdapter";
    private ArrayList<Message> mMessageTexts = new ArrayList<>();
    private Context mContext;

    public MessageRecyclerViewAdapter(Context mContext, ArrayList<Message> mMessageTexts) {
        this.mMessageTexts = mMessageTexts;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_listitem, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return mMessageTexts.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Message message = mMessageTexts.get(i);
        byte[] byteArray = Base64.decode(message.getContent(), 0);
        String messageText = new String(byteArray);
        if (message.getSender().getSenderId() == Integer.parseInt(SaveSharedPreference.getUserId(mContext))){
            viewHolder.MessageImage.setVisibility(View.GONE);
            viewHolder.incomingMessageText.setVisibility(View.GONE);
            viewHolder.incomingUsername.setVisibility(View.GONE);
            viewHolder.outgoingMessageText.setText(messageText);
        } else {
            viewHolder.MessageImage.setImageResource(R.mipmap.ic_launcher);
            viewHolder.incomingMessageText.setText(messageText);
            viewHolder.incomingUsername.setText(message.getSender().getSenderUsername());
            viewHolder.outgoingMessageText.setVisibility(View.GONE);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView MessageImage;
        TextView incomingMessageText;
        TextView outgoingMessageText;
        TextView incomingUsername;

        public ViewHolder(View itemView) {
            super(itemView);
            this.MessageImage = itemView.findViewById(R.id.imageView);
            this.incomingMessageText = itemView.findViewById(R.id.incomingTextView);
            this.outgoingMessageText = itemView.findViewById(R.id.outgoingTextView);
            this.incomingUsername = itemView.findViewById(R.id.incoming_message_username);
        }
    }
}