package com.example.zpravamessenger.zprava;

public class Message {
    private int message;
    private int conversation;
    private Sender sender;
    private String kind;
    private String content;
    private String created;
    private String updated;

    public Message(int message, int conversation, Sender sender, String kind, String content, String created, String updated) {
        this.message = message;
        this.conversation = conversation;
        this.sender = sender;
        this.kind = kind;
        this.content = content;
        this.created = created;
        this.updated = updated;
    }

    public Sender getSender() {
        return sender;
    }

    public String getContent() {
        return content;
    }

    public class Sender {
        private int id;
        private String username;

        public Sender(int id, String username) {
            this.id = id;
            this.username = username;
        }

        public int getSenderId() {
            return this.id;
        }

        public String getSenderUsername() {
            return this.username;
        }
    }
}
