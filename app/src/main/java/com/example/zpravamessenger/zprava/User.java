package com.example.zpravamessenger.zprava;

public class User {
    private int id;
    private String username;
    private String first_name;
    private String last_name;
    private String phone;
    private String email;
    private String created;
    private String updated;
    private String deleted;

    public User(String username, String email, String phone, String first_name, String last_name,
                int id, String created, String updated, String deleted){
        this.id = id;
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone = phone;
        this.email = email;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public int getUserId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }
}
