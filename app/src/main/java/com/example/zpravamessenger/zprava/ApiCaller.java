package com.example.zpravamessenger.zprava;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.zpravamessenger.zprava.activities.MessageActivity;
import com.example.zpravamessenger.zprava.activities.ConversationListActivity;
import com.example.zpravamessenger.zprava.activities.LoginActivity;
import com.example.zpravamessenger.zprava.activities.NewConversationActivity;
import com.example.zpravamessenger.zprava.activities.SignupActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ApiCaller {
    private final String djangoBaseUrl = "http://snowcrash.winny:8000/api/v1/";
    private RequestQueue queue;

    public ApiCaller(Context context) {
        this.queue = Volley.newRequestQueue(context);
    }

    public void userLogin(final LoginActivity activity, final Map<String, String> userData) {
        String requestUrl = djangoBaseUrl + "user/log_in";
        StringRequest loginRequest = new StringRequest(Request.Method.POST, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        try {
                            JSONObject innerData = new JSONObject(response);
                            innerData = innerData.getJSONObject("data");
                            activity.setAuthToken(innerData.getString("token"));
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError(error.toString());
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            protected Map<String, String> getParams() {
                Map<String, String>  params = userData;
                return params;
            }
        };
        queue.add(loginRequest);
    }

    // Need different login method due to differing activities for new user auto_login
    public void autoLogin(final SignupActivity activity, final Map<String, String> userData) {
        String requestUrl = djangoBaseUrl + "user/log_in";
        StringRequest loginRequest = new StringRequest(Request.Method.POST, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        try {
                            JSONObject innerData = new JSONObject(response);
                            innerData = innerData.getJSONObject("data");
                            activity.setAuthToken(innerData.getString("token"));
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError(error.toString());
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            protected Map<String, String> getParams() {
                Map<String, String>  params = userData;
                return params;
            }
        };
        queue.add(loginRequest);
    }

    //Need different method due to coming from a different activity
    public void getUserInfoSignup(final SignupActivity activity) {
        String requestUrl = djangoBaseUrl + "user/me";
        StringRequest loginRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        Gson gson = new Gson();
                        TypeToken<UserJsonResponse> token = new TypeToken<UserJsonResponse>(){};
                        UserJsonResponse userJson = gson.fromJson(response, token.getType());
                        activity.setUserId(userJson.data);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError(error.toString());
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }
        };
        queue.add(loginRequest);
    }

    public void getUserInfo(final LoginActivity activity) {
        String requestUrl = djangoBaseUrl + "user/me";
        StringRequest loginRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        Gson gson = new Gson();
                        TypeToken<UserJsonResponse> token = new TypeToken<UserJsonResponse>(){};
                        UserJsonResponse userJson = gson.fromJson(response, token.getType());
                        activity.setUserId(userJson.data);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError(error.toString());
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }
        };
        queue.add(loginRequest);
    }

    public void logOut(final ConversationListActivity activity) {
        String requestUrl = djangoBaseUrl + "user/log_out";
        StringRequest loginRequest = new StringRequest(Request.Method.POST, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        activity.logoutSuccess();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.logoutFailure();
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }
        };
        queue.add(loginRequest);
    }


    public void createUser(final SignupActivity activity, final Map<String, String> userData){
        String requestUrl = djangoBaseUrl + "user";
        StringRequest newUserRequest = new StringRequest(Request.Method.POST, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        try {
                            JSONObject innerData = new JSONObject(response);
                            innerData = innerData.getJSONObject("data");
                            Gson gson = new Gson();
                            User user = gson.fromJson(innerData.toString(), User.class);
                            activity.callAutoLogin(user);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError(error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String>  params = userData;
                return params;
            }
        };
        queue.add(newUserRequest);
    }

    public void getConversations(final ConversationListActivity activity) {
        String requestUrl = djangoBaseUrl + "conversation";
        StringRequest getRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        Gson gson = new Gson();
                        TypeToken<ConversationJsonResponse> token = new TypeToken<ConversationJsonResponse>(){};
                        ConversationJsonResponse conversationJson = gson.fromJson(response, token.getType());
                        activity.loadRecyclerViewData(conversationJson.data.items);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError();
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }
        };
        queue.add(getRequest);
    }

    public void getMessages(final MessageActivity activity, int chatID) {
        String requestUrl = djangoBaseUrl + "conversation/" + Integer.toString(chatID) + "/message";
        StringRequest getRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        Gson gson = new Gson();
                        TypeToken<MessageJsonResponse> token = new TypeToken<MessageJsonResponse>(){};
                        MessageJsonResponse messageJson = gson.fromJson(response, token.getType());
                        activity.loadRecyclerViewData(messageJson.data.items);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR","error => "+error.toString() );
                        activity.onError();
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }
        };
        queue.add(getRequest);
    }

    public void sendMessage(final MessageActivity activity, final int conversationID, final String type, final String encodedMessage) {
        String requestUrl = djangoBaseUrl + "conversation/" + Integer.toString(conversationID) + "/message";
        StringRequest getRequest = new StringRequest(Request.Method.POST, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        Gson gson = new Gson();
                        TypeToken<SingleMessageJsonResponse> token = new TypeToken<SingleMessageJsonResponse>(){};
                        SingleMessageJsonResponse messageJson = gson.fromJson(response, token.getType());
                        activity.messageSuccess(messageJson.data);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = null;
                        if(error.networkResponse.data!=null) {
                            try {
                                body = new String(error.networkResponse.data,"UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("ERROR", "error body => " + body);
                        Log.d("ERROR","error => "+error.toString() + ", " + error.getMessage());
                        activity.messageFailure();
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError{
                String httpPostBody="{\"sender\":{\"id\":" + SaveSharedPreference.getUserId(activity.getApplicationContext()) + "},";
                httpPostBody += "\"conversation\":" + Integer.toString(conversationID) + ",";
                httpPostBody += "\"kind\":\"" + type + "\",";
                httpPostBody += "\"content\":\"" + encodedMessage + "\"}";
                try {
                    return httpPostBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        queue.add(getRequest);
    }

    public void createConversation(final NewConversationActivity activity,final ArrayList<String> users, final String conversationName) {
        String requestUrl = djangoBaseUrl + "conversation";
        StringRequest postRequest = new StringRequest(Request.Method.POST, requestUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("VOLLEY", response);
                        activity.onSuccess();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = null;
                        if(error.networkResponse.data!=null) {
                            try {
                                body = new String(error.networkResponse.data,"UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("ERROR", "error body => " + body);
                        Log.d("ERROR","error => "+error.toString() + ", " + error.getMessage());
                    }
                }
        ) {
            //This is where you add headers to the Http request
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + SaveSharedPreference.getAuthToken(activity.getApplicationContext()));

                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError{
                String httpPostBody="{\"name\":\"" + conversationName + "\",\"users\":";
                httpPostBody += "[";
                for (String u : users) {
                    httpPostBody += "{\"username\":\"" + u + "\"},";
                }
                httpPostBody = httpPostBody.substring(0, httpPostBody.length() - 1) + "]}";
                try {
                    return httpPostBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        queue.add(postRequest);
    }

    public class ConversationJsonResponse {
        public itemsJson data;

        public class itemsJson {
            public ArrayList<Conversation> items;
        }
    }

    public class MessageJsonResponse {
        public itemsJson data;


        public class itemsJson {
            public ArrayList<Message> items;
        }
    }

    public class SingleMessageJsonResponse {
        public Message data;
    }

    public class UserJsonResponse {
        public User data;
    }
}
