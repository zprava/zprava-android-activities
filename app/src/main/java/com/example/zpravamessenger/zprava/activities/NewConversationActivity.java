package com.example.zpravamessenger.zprava.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zpravamessenger.zprava.ApiCaller;
import com.example.zpravamessenger.zprava.Message;
import com.example.zpravamessenger.zprava.NewConversationRecyclerViewAdapter;
import com.example.zpravamessenger.zprava.R;
import com.example.zpravamessenger.zprava.MessageRecyclerViewAdapter;
import com.example.zpravamessenger.zprava.SaveSharedPreference;

import java.util.ArrayList;

public class NewConversationActivity extends AppCompatActivity {
    private ArrayList<String> mUsernames;
    private NewConversationRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("notificationType");  //get the type of message from MyGcmListenerService 1 - lock or 0 -Unlock
            if (type.equals("msg")) // 1 == lock
            {
                Toast.makeText(getApplication(), "New Message from " + intent.getStringExtra("sender") +" in conversation " + intent.getStringExtra("conversationName"), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplication(), "There has been a change to your conversations.", Toast.LENGTH_LONG).show() ;
            }
        }
    };
    private ApiCaller apiCaller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_conversation);

        apiCaller = new ApiCaller(this);
        recyclerView = findViewById(R.id.newConversationRecycler);
        mUsernames = new ArrayList<>();
        loadRecyclerViewData();
        LocalBroadcastManager.getInstance(NewConversationActivity.this).registerReceiver(
                broadcastReceiver, new IntentFilter("com.example.zpravamessaenger.zprava.action.MESSAGE_RECEIVED"));
    }

    private void loadRecyclerViewData(){
        adapter = new NewConversationRecyclerViewAdapter(this, mUsernames);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void newConversationAddUser(View view) {
        TextView usernameTextView = findViewById(R.id.addToConversationTextView);
        mUsernames.add(usernameTextView.getText().toString());
        usernameTextView.setText("");
        loadRecyclerViewData();
    }

    public void createNewConversationFunction(View view) {
        EditText newConvoNameEditText = findViewById(R.id.newConvoNameEditText);
        String newConvoName = newConvoNameEditText.getText().toString();
        if (newConvoName.equals("") || mUsernames.isEmpty()) {
            Toast toast = Toast.makeText(this, "You must enter a Conversation name and add users!", Toast.LENGTH_LONG);
            toast.show();
        } else {
            mUsernames.add(SaveSharedPreference.getUserName(this));
            apiCaller.createConversation(this, mUsernames, newConvoName);
        }
    }

    public void onSuccess() {
        Intent intent = new Intent(this, ConversationListActivity.class);
        startActivity(intent);
    }

    public void exitNewConversation(View view) {
        Intent intent = new Intent(this, ConversationListActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //registerReceiver(statusReceiver,mIntent);
        LocalBroadcastManager.getInstance(NewConversationActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("com.example.zpravamessaenger.zprava.action.MESSAGE_RECEIVED"));
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }
}
